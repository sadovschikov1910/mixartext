//
//  ViewController.swift
//  mixarTest
//
//  Created by Андрей Садовщиков on 25.04.2023.
//

import UIKit
import SceneKit
import ARKit
import GLTFSceneKit

class ViewController: UIViewController {
    
    @IBOutlet var sceneView: ARSCNView!
    
    let screenSize: CGRect = UIScreen.main.bounds
    
    var buttonLeft = UIButton()
    var buttonRight = UIButton()
    var buttonUp = UIButton()
    var buttonDown = UIButton()
    
    var arActive = false;
    
    var arTargetFoundCoeff = 1.0
    var modelMoveValue = 0.05

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLighting()
        addTapGestureToSceneView()
        downloadImageTask()
        
        createButtons()
        buttonHidden(val: true)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func loadedImagesFromDirectoryContents() -> Set<ARReferenceImage>?{
        
        var index = 0
        var customReferenceSet = Set<ARReferenceImage>()
        let documentsDirectory = getDocumentsDirectory()
        
        do {
            
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil, options: [])
            let filteredContents = directoryContents.filter{ $0.pathExtension == "jpg" }
            filteredContents.forEach { (url) in
                
                do{
                    //1. Create A Data Object From Our URL
                    let imageData = try Data(contentsOf: url)
                    guard let image = UIImage(data: imageData) else { return }
                    
                    //2. Convert The UIImage To A CGImage
                    guard let cgImage = image.cgImage else { return }
                    
                    //4. Create A Custom AR Reference Image With A Unique Name
                    let customARReferenceImage = ARReferenceImage(cgImage, orientation: CGImagePropertyOrientation.up, physicalWidth: 8)
                    customARReferenceImage.name = "target"
                    
                    //4. Insert The Reference Image Into Our Set
                    customReferenceSet.insert(customARReferenceImage)
                    
                    index += 1
                    
                }catch{
                }
            }
            
        } catch {
        }
        
        return customReferenceSet
    }
    
    func downloadImageTask() {
        guard let url = URL(string: LinkTarget) else { return }
        
        let downloadSession = URLSession(configuration: URLSession.shared.configuration, delegate: self, delegateQueue: nil)
        let downloadTask = downloadSession.downloadTask(with: url)
        downloadTask.resume()
    }
    
    func createButtons() {
        createButton(button: buttonLeft, title: "Left", action: #selector(leftBtnAction), xPos: 0)
        createButton(button: buttonRight, title: "Right", action: #selector(rightBtnAction), xPos: (screenSize.width/4))
        createButton(button: buttonUp, title: "Up", action: #selector(upBtnAction), xPos:  (screenSize.width/2))
        createButton(button: buttonDown, title: "Down", action: #selector(downBtnAction), xPos: (screenSize.width/4) * 3)
    }
    
    func createButton (button: UIButton, title: String, action: Selector, xPos: CGFloat) {
        button.setTitle(title, for: .normal)
        button.tintColor = .white
        button.backgroundColor = .red
        button.addTarget(self, action: action, for: .touchUpInside)
        
        button.frame = CGRect(x: xPos + 5, y: screenSize.height - 110 , width: (screenSize.width/4) - 10, height: 50)
        
        self.view.addSubview(button)
    }
    
    func buttonHidden(val: Bool) {
        buttonLeft.isHidden = val
        buttonRight.isHidden = val
        buttonUp.isHidden = val
        buttonDown.isHidden = val
    }
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpSceneView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    
    func setUpSceneView() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        
        guard let trackedImages = loadedImagesFromDirectoryContents() else {
            print("No images available")
            return
        }
        
        configuration.detectionImages = trackedImages
        configuration.maximumNumberOfTrackedImages = 1
        sceneView.session.run(configuration)
        sceneView.showsStatistics = true
        
        sceneView.delegate = self
    }
    
    
    @objc func delCubeToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let searchNode : [SCNHitTestResult] = (self.sceneView?.hitTest(recognizer.location(ofTouch: 0, in: self.sceneView), options: nil))!
        guard let firstNode  = searchNode.first else {
            return
        }
        
        if firstNode.node.name == "cube" {
            firstNode.node.removeFromParentNode()
        }
    }
    
    @objc func addCubeToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        
        let tapLocation = recognizer.location(in: sceneView)
        guard let query = sceneView.raycastQuery(from: tapLocation, allowing: .existingPlaneInfinite, alignment: .any) else {
            return
        }
        
        let results = sceneView.session.raycast(query)
        
        guard let hitTestResult = results.first else { return }
        let translation = hitTestResult.worldTransform.translation
        let x = translation.x
        let y = translation.y
        let z = translation.z
        
        let searchNode : [SCNHitTestResult] = (self.sceneView?.hitTest(recognizer.location(ofTouch: 0, in: self.sceneView), options: nil))!
        guard let firstNode  = searchNode.first else {
            return
        }
        
        if firstNode.node.name == "cube" {
            firstNode.node.geometry?.firstMaterial?.diffuse.contents = UIColor.random;
            return
        }
        
        let boxGeometry = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)
        let material = SCNMaterial()
        
        let boxNode = SCNNode(geometry: boxGeometry)
        boxNode.geometry?.materials = [material]
        
        boxNode.position = SCNVector3(x,y,z)
        boxNode.name = "cube";
        sceneView.scene.rootNode.addChildNode(boxNode)
    }
    
    func addTapGestureToSceneView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.addCubeToSceneView(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self,action: #selector(ViewController.delCubeToSceneView(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(longPressRecognizer)
    }
    
    
    func session(_ session: ARSession, didFailWithError error: Error) {
    }
    
    @objc func leftBtnAction() {
        for node in sceneView.scene.rootNode.childNodes {
            node.position.x = node.position.x - Float(modelMoveValue * arTargetFoundCoeff)
        }
    }
    
    @objc func rightBtnAction() {
        for node in sceneView.scene.rootNode.childNodes {
            node.position.x = node.position.x + Float(modelMoveValue * arTargetFoundCoeff)
        }
    }
    
    @objc func upBtnAction() {
        for node in sceneView.scene.rootNode.childNodes {
            node.position.z = node.position.z - Float(modelMoveValue * arTargetFoundCoeff)
        }
    }
    
    @objc func downBtnAction() {
        for node in sceneView.scene.rootNode.childNodes {
            node.position.z = node.position.z + Float(modelMoveValue * arTargetFoundCoeff)
        }
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
    }
}


extension UIColor {
    static var random: UIColor {
        return .init(hue: .random(in: 0...1), saturation: 1, brightness: 1, alpha: 1)
    }
}

extension float4x4 {
    var translation: SIMD3<Float> {
        let translation = self.columns.3
        return SIMD3(translation.x, translation.y, translation.z)
    }
}



extension ViewController: ARSCNViewDelegate {
    
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
  
        arActive = true
        let node = SCNNode()
        
        if let imageAnchor = anchor as? ARImageAnchor {
            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            plane.firstMaterial?.diffuse.contents = UIColor(white: 1, alpha: 0)
            
            let planeNode = SCNNode(geometry: plane)
            planeNode.eulerAngles.z = .pi * 2
            
            let myURL = NSURL(string: LinkModel)
            DispatchQueue.main.async {
                var shipScene: SCNScene
                
                let sceneSource =  GLTFSceneSource(url: myURL! as URL)
                shipScene = try! sceneSource.scene()
                
                let shipNode = shipScene.rootNode
                shipNode.position = SCNVector3Zero
                shipNode.position.z = 0.15
                
                planeNode.addChildNode(shipNode)
                
                node.addChildNode(planeNode)
            }
        }
        sceneView.scene.rootNode.addChildNode(node)
        return node
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let width = CGFloat(planeAnchor.planeExtent.width)
        let height = CGFloat(planeAnchor.planeExtent.height)
        let plane = SCNPlane(width: width, height: height)
        
        plane.materials.first?.diffuse.contents = UIColor.clear;
        
        let planeNode = SCNNode(geometry: plane)
        
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2
        
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        if let imageAnchor = anchor as? ARImageAnchor, imageAnchor.isTracked == false {
            arTargetFoundCoeff = 1.0
        }
        
        if let imageAnchor = anchor as? ARImageAnchor, imageAnchor.isTracked == true {
            arTargetFoundCoeff = 2.0
        }
        
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
              let planeNode = node.childNodes.first,
              let plane = planeNode.geometry as? SCNPlane
        else { return }
        
        let width = CGFloat(planeAnchor.planeExtent.width)
        let height = CGFloat(planeAnchor.planeExtent.height)
        plane.width = width
        plane.height = height
        
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x, y, z)
        
        DispatchQueue.main.async {
            if self.sceneView.scene.rootNode.childNodes.count > (self.arActive ? 4 : 3) && self.buttonLeft.isHidden {
                self.buttonHidden(val: false)
            }
            if self.sceneView.scene.rootNode.childNodes.count <= (self.arActive ? 4 : 3) && !self.buttonLeft.isHidden {
                self.buttonHidden(val: true)
            }
        }
        
    }
}

extension  ViewController: URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        let fileURL = getDocumentsDirectory().appendingPathComponent("image.jpg")
        do {
            try FileManager.default.copyItem(at: location, to: fileURL)
            
            print("Successfuly Saved File \(fileURL)")
            
        } catch {
            print("Error Saving: \(error)")
        }
        
    }
}
